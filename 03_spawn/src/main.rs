use async_std::task;
use async_std::task::sleep;
use async_std::task::spawn;
use std::time::Duration;

async fn thing(d: u64) {
    let dur = Duration::from_secs(d);
    println!("Sleeping for {}", d);
    sleep(dur).await;
    println!("Slept for {}", d);
}

async fn do_things() {
    let range = 1..=5;

    let fs: Vec<_> = range
        .rev()
        .map(|n| spawn(async move { thing(n).await }))
        .collect();

    let dur = Duration::from_secs(2);
    sleep(dur).await;
    println!("=========");

    for f in fs {
        f.await
    }
}

fn main() {
    let fut = do_things();

    task::block_on(fut);
}
