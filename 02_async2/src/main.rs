use async_std::task;
use async_std::task::sleep;
use futures::future::join_all;
use std::time::Duration;

async fn thing(d: u64) {
    let dur = Duration::from_secs(d);
    println!("Sleeping for {}", d);
    sleep(dur).await;
    println!("Slept for {}", d);
}

async fn do_things() {
    let range = 1..=5;
    let futs = range.rev().map(|n| thing(n));
    join_all(futs).await;
}

fn main() {
    let fut = do_things();

    task::block_on(fut);
}
