use std::thread::sleep;
use std::time::Duration;

fn thing(d: u64) {
    let dur = Duration::from_secs(d);
    println!("Sleeping for {}", d);
    sleep(dur);
    println!("Slept for {}", d);
}

fn main() {
    let range = 1..=5;
    for n in range.rev() {
        thing(n);
    }
}
